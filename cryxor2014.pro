#-------------------------------------------------
#
# Project created by QtCreator 2013-11-28T17:35:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cryxor2014
TEMPLATE = app


SOURCES  += main.cpp

HEADERS  +=

FORMS    +=

unix:!macx|win32: LIBS += -L$$PWD/../../../../../home/poulet_a/projets/cryxor2014/ -lcryxor

INCLUDEPATH += $$PWD/../../../../../home/poulet_a/projets/cryxor2014
DEPENDPATH += $$PWD/../../../../../home/poulet_a/projets/cryxor2014
